# Changelog

All Notable changes to `Bouncer` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 2018-03-20
### Fixed
- Required Laravel 5.6, renamed service provider, fix style to PSR2


## NEXT - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing
