# Bouncer

Bouncer checks the system_control entry in redis for the start and end times of a promotion.

## Installation

Add this bouncer repo to your projects composer.json:
```
"repositories": [{
    "type": "vcs",
    "url": "https://bitbucket.org/haven_agency/bouncer.git"
}]
```

Then run: 
```bash
composer require "haven/bouncer:1.0.*"
```

Add to your .env:
```
SYSTEM_CONTROL_KEY=keyname.hvnln.com
```

Add to your config/database.php
```
'redis' => [

    'client' => 'predis',

    'system' => [
        'host'     => env('REDIS_HOST'),
        'password' => env('REDIS_PASSWORD'),
        'port'     => env('REDIS_PORT'),
        'database' => env('REDIS_DB', 0),
        'persistent' => true
    ],

],
```

Be sure you have an entry in the shared redis instance

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email dev@havenagency.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.