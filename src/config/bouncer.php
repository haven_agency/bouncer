<?php

return [
    'status' => env('SYSTEM_CONTROL_STATUS', true),
    'start_time' => env('SYSTEM_CONTROL_START_TIME', '2000-01-01 00:00:00'),
    'end_time' => env('SYSTEM_CONTROL_END_TIME', '5000-01-01 00:00:00'),
];
