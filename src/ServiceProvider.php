<?php

namespace Haven\Bouncer;

use Illuminate\Routing\Router;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('bouncer', \Haven\Bouncer\BouncerMiddleware::class);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__ . '/bouncer.php' => config_path('bouncer.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__.'/config/database.php',
            'database'
        );
    }
}
