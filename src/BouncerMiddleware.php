<?php

namespace Haven\Bouncer;

use App;
use Closure;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class BouncerMiddleware
{

    // Exempt routes by first segment
    // ie: /exemptName/something/something
    protected $exempt_routes = [
        'preview',
        'legal'
    ];

    /**
     * Handle an incoming request.
     * Checks whether the sweepstakes is open.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Skip bouncer for exempt routes
        if (in_array($request->segment(1), $this->exempt_routes)) {
            return $next($request);
        }

        // If env var is set to force the site open, redirect
        if (env('FORCE_OPEN')==true) {
            return $next($request);
        }

        // Pull system settings from Redis
        $redis = Redis::connection('system');
        $system = collect($redis->hGetAll(env('SYSTEM_CONTROL_KEY')));

        // Is the site globally disabled?
        if (!$system->get('status') && $request->path() != 'closed') {
            return redirect('closed');
        }

        $now = time();

        // Is the sweepstakes not open yet?
        $start_json = $system->get('start');
        if (!$start_json) {
            abort(500);
        }

        $start = json_decode($start_json)->epoch;
        if ($start > $now && $request->path() != 'not-open') {
            return redirect('not-open');
        }
        
        // Is the sweepstakes closed?
        $end_json = $system->get('end');
        if (!$end_json) {
            abort(500);
        }
        $end = json_decode($end_json)->epoch;
        if ($end <= $now && $request->path() != 'closed') {
            return redirect('closed');
        }

        // If we're on the 'closed' or 'not open' page but the site is now open, redirect to home
        if ($system->get('status') == 1 &&
            (
                ($request->path() == 'not-open' && $start <= $now) ||
                ($request->path() == 'closed' && $end > $now)
            )
        ) {
            return redirect('/');
        }

        return $next($request);
    }
}
